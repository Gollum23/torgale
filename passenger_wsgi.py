#!$HOME/local/bin/python
# -*- coding: utf-8 -*-
INTERP = "/home/torgale/local/bin/python"
DEBUG = False

import sys
import os
if sys.executable != INTERP: os.execl(INTERP, INTERP, *sys.argv)
sys.path.append(os.getcwd())
os.environ['DJANGO_SETTINGS_MODULE'] = "torgale.settings"
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

if DEBUG:
	from paste.exceptions.errormiddleware import ErrorMiddleware
	application = ErrorMiddleware(application, debug=True)