/* TEAM */
    Creator/Designer/Overlord: mantistecnologia.co
    Site: http://www.mantistecnologia.co
    Location: Bogota - Colombia

/* SITE */
    Last update:2013/01/27
    Standards: HTML5, CSS3, JavaScript, Python, Django
    Libraries: jQuery
    Plugins: Infinite-scroll, flexslider
    Language:Español
    Doctype:HTML5
    IDE: PyCharm 2.6.3, Filezilla, git, bitbucket,