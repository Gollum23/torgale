__author__ = 'Gollum23'

from django.contrib.sitemaps import Sitemap
from web.models import Inmueble


class SitemapTorgale(Sitemap):
    def __init__(self, name):
        self.names = name

    def items(self):
        return self.names

    def changefreq(self):
        return 'monthly'

    def location(self, obj):
        return obj


class OfertaSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        return Inmueble.objects.filter(activo='si')

    def location(self, obj):
        return '/detalle/' + str(obj)


class StaticSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5
    lastmod = None

    def items(self):
        return [
            "/",
            "/empresa",
            "/servicios",
            ("/inmuebles", 'weekly'),
            "/contacto",
        ]

    def location(self, obj):
        return obj[0] if isinstance(obj, tuple) else obj

    def changefreq(self, obj):
        return obj[1] if isinstance(obj, tuple) else "never"