# encoding: utf-8
from django.core.mail import EmailMessage, BadHeaderError
from django.template import RequestContext
from django.shortcuts import render_to_response, render
from django.http import Http404, HttpResponse
from django_easyfilters import FilterSet

from models import *
from forms import ContactForm

#from django_easyfilters.filters import ValuesFilter


def index(request):
    img_valor = 'img_index'
    try:
        imagenes = Slider.objects.all()
    except Slider.DoesNotExist:
        raise Http404
    return render_to_response('index.html',
                              {'slider': imagenes, 'imgvalor': img_valor})


def empresa(request):
    img_valor = 'img_empresa'
    return render_to_response('empresa.html',
                              {'imgvalor': img_valor})


def servicios(request):
    img_valor = 'img_servicios'
    return render_to_response('servicios.html',
                              {'imgvalor': img_valor})


def inmuebles(request):
    #f = InmuebleFilter(request.GET, queryset=Inmueble.objects.all())
    #return render(request,'inmuebles.html',{'filtro':f})
    img_valor = 'img_inmuebles'
    f = Inmueble.objects.filter(activo='si')
    inmueblesfilter = InmFilterSet(f, request.GET)
    return render(request, 'inmuebles.html',
                  {'inmuebles': inmueblesfilter.qs,
                   'filtros': inmueblesfilter,
                   'imgvalor': img_valor})


class InmFilterSet(FilterSet):
    fields = ['zona', 'claseInmueble', 'negocio', ]
    title_fields = ['zona', 'claseInmueble', 'negocio', ]


def detalle(request, codigo):
    try:
        img_valor = 'img_detalles'
        inm = Inmueble.objects.get(codInmueble=codigo)
    except Inmueble.DoesNotExist:
        raise Http404
    return render(request, 'detalle.html',
                  {'inmueble': inm,
                   'imgvalor': img_valor})


def contacto(request):
    messaget = None
    img_valor = 'img_contacto'
    form = ContactForm()
    if request.method == 'POST':
        form = ContactForm(request.POST)
        fullname = request.POST.get('fullname', '')
        phone = request.POST.get('phone', '')
        subject = request.POST.get('subject', '')
        email = request.POST.get('email', '')
        message = request.POST.get('message', '')
        fullmessage = 'Nombre: ' + fullname + '<br>' + 'Telefono: ' \
                      '' + phone + '<br>' + 'email: ' \
                      + email + '<br>' + message
        if form.is_valid():
            try:
                mail = EmailMessage(subject, fullmessage, email,
                                    ['arrendamientos@torgale.com.co'])
                mail.content_subtype = 'html'
                mail.send()
            except BadHeaderError:
                return HttpResponse('Cabezera invalida')
            messaget = '¡Gracias! nos pondremos en ' \
                       'contacto con usted muy pronto'
            form = ContactForm()
    return render_to_response('contact.html',
                              {'contacform': form,
                               'imgvalor': img_valor,
                               'messaget': messaget},
                              context_instance=RequestContext(request))