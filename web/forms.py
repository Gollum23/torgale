# encoding: utf-8
__author__ = 'Gollum23'

from django import forms


class ContactForm(forms.Form):
    fullname = forms.RegexField(
        label='Nombre Completo',
        max_length=100,
        regex=r'^[a-zA-Z ]+$',
        error_message='Solo se permiten letras'
    )
    phone = forms.IntegerField(
        error_messages={
            'invalid': 'Ingrese un numero telefonico valido'}
    )
    subject = forms.CharField(max_length=200)
    email = forms.EmailField(
        error_messages={
            'invalid': 'Ingrese una dirección valida'}
    )
    message = forms.CharField(
        widget=forms.Textarea, max_length=1000)
