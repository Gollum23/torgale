__author__ = 'Gollum23'
from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from web.models import Inmueble

@dajaxice_register
def filtrarInmueble(request, fzona='',):
    dajax = Dajax()
    inmuebles = Inmueble.objects.filter(zona__contains=fzona)
    for o in inmuebles:
        print o.codInmueble
    dajax.assign('#mensaje','innerHTML',o.codInmueble)
    return dajax.json()

@dajaxice_register
def assign_test(request):
    dajax = Dajax()
    dajax.assign('#mensaje', 'innerHTML', 'Hello World!')
    #dajax.add_css_class('div .alert', 'red')
    return dajax.json()
