# encoding: utf-8
from django.db import models
from ckeditor.fields import RichTextField

optgarage = (('si', 'Si'), ('no', 'No'), ('comunal', 'Comunal'),)
opthorizontal = (('si', 'Si'), ('no', 'No'), ('n/a', 'N/A'),)
optconjunto = (('si', 'Si'), ('no', 'No'),)
optascensor = (('si', 'Si'), ('no', 'No'),)
optadmin = (('si', 'Si'), ('no', 'No'), ('n/a', 'N/A'),)
optnegocio = (('arriendo', 'Arriendo'), ('venta', 'Venta'),)
optactivo = (('si', 'Si'), ('no', 'No'),)
optpiso = (('alfombra', 'Alfombra'), ('baldosa', 'Baldosa'), ('ceramica', 'Ceramica'), ('madera',
                                                                'Madera'),
           ('porcelanato', 'Porcelanato'), ('vinisol', 'Vinisol'), ('n/a',
                                                                    'N/A'),)


class Slider(models.Model):
    def folder_slider_img(self, name):
        folder = 'slider/' + name
        return folder

    def admin_thumbs(self):
        return '<img src="/media/%s" width="200">' % self.imagen
    admin_thumbs.allow_tags = True
    admin_thumbs.short_description = "Imagen"

    def caption_parser(self):
        return u'%s' % self.caption
    caption_parser.allow_tags = True
    caption_parser.short_description = "Caption"

    caption = RichTextField(verbose_name='Caption', blank=True)
    imagen = models.FileField(upload_to=folder_slider_img,
                              verbose_name='Imagen')
    title = models.CharField(max_length=255, verbose_name='Titulo')


class ZonaInmueble(models.Model):
    zona = models.CharField(max_length=24, verbose_name='Zona', unique=True)

    def __unicode__(self):
        return u'%s' % self.zona


class TipoInmueble(models.Model):
    descriptionTipo = models.CharField(max_length=24,
                                       verbose_name='Tipo de inmueble',
                                       unique=True)

    def __unicode__(self):
        return u'%s' % self.descriptionTipo


class Cliente(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nombre', unique=True)
    phoneday = models.CharField(max_length=24, verbose_name='Teléfono día')
    phonenight = models.CharField(max_length=24, verbose_name='Teléfono noche')
    celphone = models.CharField(max_length=24, verbose_name='Celular')
    email = models.EmailField(max_length=50, verbose_name='Correo Electronico')

    def __unicode__(self):
        return u'%s' % self.name


class Inmueble(models.Model):
    def admin_cliente(self):
        return u'%s' % self.cliente

    admin_cliente.short_description = 'Nombre del cliente'
    codInmueble = models.CharField(max_length=24,
                                   verbose_name='Codigo del inmueble',
                                   unique=True)
    cliente = models.ForeignKey(Cliente, to_field='name',
                                verbose_name='Cliente')
    activo = models.CharField(max_length=6, verbose_name='Activo',
                              choices=optactivo)
    negocio = models.CharField(max_length=10, verbose_name='Tipo de negocio',
                               choices=optnegocio)
    zona = models.ForeignKey(ZonaInmueble, verbose_name='Zona')
    direccion = models.CharField(max_length=100, verbose_name='Dirección')
    claseInmueble = models.ForeignKey(TipoInmueble,
                                      verbose_name='Clase de inmueble')
    areac = models.CharField(max_length=25,
                             verbose_name='Area Construida (en '
                             'metros cuadrados')
    areat = models.CharField(max_length=25, verbose_name='Area Total (en '
                                                         'metros cuadrados')
    habitaciones = models.CharField(max_length=2,
                                    verbose_name='No de habitaciones')
    closets = models.CharField(max_length=4, verbose_name='No de closets')
    banos = models.CharField(max_length=2, verbose_name='No de baños')
    numeroPiso = models.CharField(max_length=3,
                                  verbose_name='No de piso(si aplica)',
                                  blank=True)
    cantPisos = models.CharField(max_length=3, verbose_name='No de Pisos',
                                 blank=True)
    garaje = models.CharField(max_length=7, choices=optgarage,
                              verbose_name='Garaje')
    horizontal = models.CharField(max_length=6, choices=opthorizontal,
                                  verbose_name='Propiedad Horizontal')
    conjunto = models.CharField(max_length=6, choices=optconjunto,
                                verbose_name='Conjunto Cerrado')
    estrato = models.CharField(max_length=1, verbose_name='Estrato')
    ascensor = models.CharField(max_length=6, verbose_name='Ascensor',
                                choices=optascensor)
    venta = models.IntegerField(max_length=24, verbose_name='Valor de Venta',
                                blank=True, null=True)
    arriendo = models.IntegerField(max_length=24,
                                   verbose_name='Canon de arrendamiento',
                                   blank=True, null=True)
    administracion = models.IntegerField(max_length=24,
                                         verbose_name=
                                         'Valor de la administracion',
                                         blank=True, null=True)
    adminIncluida = models.CharField(max_length=6, choices=optadmin,
                                     verbose_name='Administración incluida')
    barrio = models.CharField(max_length=24, verbose_name='Barrio')
    contruccion = models.CharField(max_length=24,
                                   verbose_name='Tiempo de contrucción')
    cocina = models.CharField(max_length=6, verbose_name='Cocina Integral',
                              choices=optactivo)
    pisoalcobas = models.CharField(max_length=24,
                                   verbose_name='Tipo de piso alcobas',
                                   choices=optpiso)
    pisocomedor = models.CharField(max_length=24,
                                   verbose_name='Tipo de piso comedor',
                                   choices=optpiso)
    pisosala = models.CharField(max_length=24,
                                verbose_name='Tipo de piso sala',
                                choices=optpiso)
    serviciogas = models.CharField(max_length=6,
                                   verbose_name='Servicio de gas',
                                   choices=optactivo)
    observaciones = RichTextField(verbose_name='Observaciones')
    lat = models.CharField(max_length=24, verbose_name='Latitud')
    lon = models.CharField(max_length=24, verbose_name='Longitud')

    def __unicode__(self):
        return u'%s' % self.codInmueble


class ImgsInmueble(models.Model):
    def folderImg(self, name):
        folder = 'inmuebles/' + self.inmueble.codInmueble + '/' + name
        return folder

    def thumbs_fotos(self):
        return '<img src="/media/%s" width="200"' % self.imagen
    thumbs_fotos.allow_tags = True
    thumbs_fotos.short_description = 'Miniatura'

    def description_parser(self):
        return u'%s' % self.descripcion
    description_parser.allow_tags = True
    description_parser.short_description = 'Descripción'

    inmueble = models.ForeignKey(Inmueble, to_field='codInmueble',
                                 verbose_name='Codigo del Inmueble', related_name='images')
    imagen = models.FileField(upload_to=folderImg, verbose_name='Foto')
    descripcion = RichTextField(verbose_name='Descripción')
    portada = models.CharField(max_length=6, verbose_name='Usar en portada',
                               choices=optactivo)

############### Filtros ###############
import django_filters


class InmuebleFilter(django_filters.FilterSet):
    #zona = django_filters.AllValuesFilter(widget=django_filters.LinkWidget)
    #claseInmueble = django_filters.AllValuesFilter(widget=django_filters
    # .LinkWidget)
    class Meta:
        model = Inmueble
        fields = ['zona', 'claseInmueble', 'habitaciones', 'arriendo']

    def __init__(self, *args, **kwargs):
        super(InmuebleFilter, self).__init__(*args, **kwargs)
        self.filters['zona'].extra.update(
            {'empty_label': u'Todas las zonas'}
        )
