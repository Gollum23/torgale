__author__ = 'Gollum23'

from web.models import Slider, TipoInmueble,\
    Inmueble, ImgsInmueble, Cliente, ZonaInmueble
from django.contrib import admin


class SliderAdmin(admin.ModelAdmin):
    list_display = ('caption_parser', 'admin_thumbs')


class TipoInmuebleAdmin(admin.ModelAdmin):
    list_display = ('descriptionTipo',)


class InmueblesAdmin(admin.ModelAdmin):
    list_display = ('codInmueble', 'negocio', 'claseInmueble', 'admin_cliente',
                    'activo')
    list_editable = ('activo', )
    list_filter = ('activo', 'claseInmueble', 'negocio', 'zona',)


class ImgsInmueblesAdmin(admin.ModelAdmin):
    list_display = ('inmueble', 'description_parser', 'thumbs_fotos')
    list_filter = ('inmueble',)


class ClientesAdmin(admin.ModelAdmin):
    list_display = ('name', 'celphone', 'phoneday', 'phonenight', 'email')


class ZonaAdmin(admin.ModelAdmin):
    list_display = ('zona',)

admin.site.register(Slider, SliderAdmin)
admin.site.register(TipoInmueble, TipoInmuebleAdmin)
admin.site.register(Inmueble, InmueblesAdmin)
admin.site.register(ImgsInmueble, ImgsInmueblesAdmin)
admin.site.register(Cliente, ClientesAdmin)
admin.site.register(ZonaInmueble, ZonaAdmin)
