from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf import settings
from web.sitemap import *
#from dajaxice.core import dajaxice_autodiscover, dajaxice_config
#dajaxice_autodiscover()
#from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', 'web.views.index', name='home'),
                       url(r'^empresa/', 'web.views.empresa', name='nosotros'),
                       url(r'^servicios/', 'web.views.servicios',
                           name='servicios'),
                       url(r'^inmuebles/', 'web.views.inmuebles',
                           name='inmuebles'),
                       url(r'^detalle/(?P<codigo>\w+)/$', 'web.views.detalle',
                           name='detalle'),
                       url(r'^contacto/', 'web.views.contacto',
                           name='contacto'),
                       # url(r'^torgale/', include('torgale.foo.urls')),

                       # Uncomment the admin/doc line below to enable admin
                       # documentation:
                       # url(r'^admin/doc/', include('django.contrib.admindocs
                       # .urls')),

                       # Uncomment the next line to enable the admin:
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^ckeditor/', include('ckeditor.urls')),
                       url(r'^media/(?P<path>.*)$',
                           'django.views.static.serve',
                           {'document_root': settings.MEDIA_ROOT, }),
                       #url(dajaxice_config.dajaxice_url,
                       # include('dajaxice.urls')),
                       #url(r'^%s' % settings.DAJAXICE_MEDIA_PREFIX,
                       # include('dajaxice.urls'))
                       )

sitemaps = {
    'static': StaticSitemap,
    'inmuebles': OfertaSitemap,
}

urlpatterns += patterns('',
                       url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views'
                                            '.sitemap', {'sitemaps': sitemaps}),
                       url(r'^humans\.txt$', TemplateView.as_view(
                         template_name='humans.txt')),
                       url(r'^robots\.txt$', TemplateView.as_view(
                         template_name='robots.txt')),
                       url(r'^channel\.html$', TemplateView.as_view(
                           template_name='channel.html')),
)